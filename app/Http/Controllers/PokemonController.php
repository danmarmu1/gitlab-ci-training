<?php

namespace App\Http\Controllers;

use Validator;
use App\Pokemon;
use Illuminate\Http\Request;

class PokemonController extends Controller {

    // Retrieve and return a list of all Pokemon.
    // In the real world this should be paginated. :)
    public function index()
    {
        return response()->json(['pokemon' => Pokemon::all()]);
    }

    // Store a new Pokemon from a POST
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'number' => 'required',
            'description' => 'required'
        ],[
            'description.required' => 'Please fill out the :attribute.'
        ]);

        $pokemon = Pokemon::create($request->all());

        return redirect()->route('pokemon.show', ['id' => $pokemon->id]);
    }

    // Show a specific Pokemon's record from a GET with an id
    public function show($id)
    {
        return response()->json(['pokemon' => Pokemon::findOrFail($id)]);
    }

    // Update a specific Pokemon's record from a PUT
    public function update(Request $request, $id)
    {
        try {
            $pokemon = Pokemon::find($id);
            
        } catch (ModelNotFoundException $e) {
            return response()->json([
            'error' => [
                'message' => 'Pokémon not found. Consider adding it!'
            ] ], 404);
        }

        $this->validate($request, [
            'name' => 'required|max:255',
            'number' => 'required',
            'description' => 'required'
        ]);

        $pokemon->name = $request->input('name');
        $pokemon->number = $request->input('number');
        $pokemon->description = $request->input('description');
        $pokemon->save();
        return response()->json($pokemon);
    }

    // Delete a Pokemon's record from a DELETE
    public function destroy($id)
    {
        $pokemon = Pokemon::find($id);
        $pokemon->delete();
        return response()->json(['pokemon' => Pokemon::all()]);
    }
}

